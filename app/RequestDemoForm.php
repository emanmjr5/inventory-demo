<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDemoForm extends Model
{
    protected $table = 'request_demo_forms';
}
