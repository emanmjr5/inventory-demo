<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Auth\Models\User;

class RequestDemoForm extends Model
{
    protected $table = 'request_demo_forms';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
