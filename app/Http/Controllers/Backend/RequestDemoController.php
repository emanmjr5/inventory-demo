<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\RequestDemoForm;
use App\Mail\RequestDemoApprove;
use App\Mail\RequestDemoReject;
use Illuminate\Support\Facades\Mail;

/**
 * Class RequestDemoController.
 */
class RequestDemoController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $requestsDemo = RequestDemoForm::paginate(10);
        return view('backend.request-demo.index', compact('requestsDemo'));
    }

    public function approveDemo($id)
    {
        $requestDemo = RequestDemoForm::find($id);
        $requestDemo->status = 'approved'; // Initial Status
        $requestDemo->save();

        if($requestDemo){
            Mail::to(auth()->user()->email)->send(new RequestDemoApprove($requestDemo));
        }

        return back()->withFlashSuccess('Successfully approved a request');
    }

    public function rejectDemo($id)
    {
        $requestDemo = RequestDemoForm::find($id);
        $requestDemo->status = 'rejected'; // Initial Status
        $requestDemo->save();

        if($requestDemo){
            Mail::to(auth()->user()->email)->send(new RequestDemoReject($requestDemo));
        }

        return back()->withFlashSuccess('Successfully rejected a request');
    }
}
