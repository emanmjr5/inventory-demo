<div>
    Hi {{ $details->user->name }},
    <br>
    <br>
    Request demo form has been rejected.
    <br>
    <br>
    Reference Number: {{ $details->reference_no }}
    <br>
    <br>
    Regards,
    <br>
    VST ECS PHIL INC.
</div>