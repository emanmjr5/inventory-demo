<div>
    Hi {{ auth()->user()->name }},
    <br>
    <br>
    Thank you for submitting your request form
    <br>
    <br>
    Reference Number: {{ $details->reference_no }}
    <br>
    <br>
    Regards,
    <br>
    VST ECS PHIL INC.
</div>