@extends('frontend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <x-frontend.card>
                    
                    <x-slot name="header">
                        Request a Demo
                    </x-slot>

                    <x-slot name="body">
                 
                        <form action="{{ route(
                            'frontend.user.request-demo') }}" method="POST">
                        {{ csrf_field() }}
                            <div class="p-3 mb-2 bg-secondary text-white">
                        
                                <label for="test_label">Activity Category</label>
                                <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0"></legend>
                                <div class="col-sm-10">
                                    <div class="form-check">
                                    <input class="form-check-input" type="radio" name="activity_category" id="gridRadios1" value="1" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                    Customer Experience Workshop
                                    </label>
                                    </div>
                                    
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios2" value="2">
                                <label class="form-check-label" for="gridRadios2">
                                    Business Partner Develeopment/Workshop
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios3" value="3">
                                <label class="form-check-label" for="gridRadios3">
                                    Virtual POC/POT
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios4" value="4">
                                <label class="form-check-label" for="gridRadios4">
                                    iSupport & Education Services
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios5" value="5">
                                <label class="form-check-label" for="gridRadios5">
                                    Internal Development/TCT
                                </label>
                                </div>

                                </div>
                                </div><br>
                            
                        
                            <div class="activity_cat_1"> 

                                <div class="p-3 mb-2 bg-info text-white">
                                    Activity Details 
                                </div>
                             
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Project Name</label>
                                    <input type="text" class="form-control" name="project_name" placeholder="Input Project Name">
                                </div>
                            
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Project Description</label>
                                    <input type="text" class="form-control" name="project_decription" placeholder="Input Project Details and Objective">
                                </div>

                                <div class="form-group row">
                                    <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                                <div class="col-10">
                                    <input class="form-control" type="date" value="2020-07-01" name="date_start" id="example-date-input">
                                </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                                <div class="col-10">
                                    <input class="form-control" type="date" value="2020-07-01" name="date_end" id="example-date-input">
                                </div>
                                </div>

                                <div class="btn-group">
                                    <label for="formGroupExampleInput">Product</label>
                                
                                    <div class="form-group col-md-12">
                                    <select id="inputState" class="form-control">
                                        <option selected></option>
                                        <option>Oracle</option>
                                        <option>Cisco</option>
                                        <option>Huawei</option>
                                        <option>VMware</option>
                                        <option>SAP</option>
                                        <option>Pivotal</option>
                                    </select>
                                    </div>
                                </div>
                                                                
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Estimated Size of Oppurtunity(PHP)</label>
                                    <input type="text" class="form-control" name="estimated_size_of_oppurtunity" placeholder="Input estimated size of oppurtunity">
                                </div>
                            </div>

                            <div class="activity_cat_2">
                            
                                <div class="p-3 mb-2 bg-info text-white">
                                    Reseller Details
                                </div>
                                
                                <div class="p-3 mb-2 bg-secondary text-white">

                                    <div class="btn-group">
                                        <label for="formGroupExampleInput">Company</label>
                                    
                                        <div class="form-group col-md-12">
                                        <select id="inputState" class="form-control">
                                            <option selected>Select your company</option>
                                            <option>Red Rock Security</option>
                                            <option>Shellsoft Technology Corporation</option>
                                            <option>Nexus Technologies, Incorporated</option>
                                            <option>Micro-D International Incorporated</option>
                                            <option>Questronix Corporation</option>
                                            <option>OneDepot</option>
                                        </select>
                                        </div>

                                    </div>

                                </div>
                                                                 
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Contact Person</label>
                                    <input type="text" class="form-control" name="partner_contact_person" placeholder="Input contact person">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Email Address</label>
                                    <input type="text" class="form-control" name="partner_email_address" placeholder="Input Email Address">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Designation</label>
                                    <input type="text" class="form-control" name="partner_designation" placeholder="Input Designation">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Mobile/Phone</label>
                                    <input type="text" class="form-control" name="partner_contact_details" placeholder="Input Contact Number">
                                </div>

                            </div>

                            <div class="activity_cat_3">

                                <div class="p-3 mb-2 bg-info text-white">
                                    Customer Details
                                </div>
                                
                                <div class="btn-group">
                                    <label for="formGroupExampleInput">Company</label>
                                
                                    <select id="inputState" class="form-control">
                                        <option selected>Select your company</option>
                                        <option>Red Rock Security</option>
                                        <option>Shellsoft Technology Corporation</option>
                                        <option>Nexus Technologies, Incorporated</option>
                                        <option>Micro-D International Incorporated</option>
                                        <option>Questronix Corporation</option>
                                        <option>OneDepot</option>
                                    </select>
                                </div>
                                

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Contact Person</label>
                                    <input type="text" class="form-control" name="customer_contact_person" placeholder="Input contact person">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Email Address</label>
                                    <input type="text" class="form-control" name="customer_email_address" placeholder="Input Email Address">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Designation</label>
                                    <input type="text" class="form-control" name="customer_designation" placeholder="Input Designation">
                                </div>

                                <div class="form-group">
                                    <label for="formGroupExampleInput">Mobile/Phone</label>
                                    <input type="text" class="form-control" name="customer_contact_details" placeholder="Input Contact Number">
                                </div>
                            </div>
                        
                                                   
                        <div class="activity_cat_4">
                            <div class="p-3 mb-2 bg-info text-white">
                                Compute Requirements
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">Compute Name</label>
                                    <input type="text" class="form-control" name="compute_1_name" placeholder="Input Compute Name">
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">vCPU(1-8)</label>
                                    <input type="text" class="form-control" name="compute_1_vcpu" placeholder="Input vCPU">
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">Memory (GB)</label>
                                    <input type="text" class="form-control" name="compute_1_memory" placeholder="Input Memory">
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">Operating System</label>
                                    <input type="text" class="form-control" name="compute_1_operating_system" placeholder="Input OS">
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">No. of NIC</label>
                                    <input type="text" class="form-control" name="compute_no_of_nic" placeholder="Input # of NIC">
                            </div>
                            
                            <div class="form-group">
                                    <label for="formGroupExampleInput">AD Required?</label>
                                    <input type="text" class="form-control" name="compute_1_ad_requirements" placeholder="Input AD Requirements">
                            </div>

                            <div class="form-group">
                                    <label for="formGroupExampleInput">Others (Pls specify)</label>
                                    <input type="text" class="form-control" name="compute_1_others" placeholder="Input others">
                            </div>
                        </div>

                        <div class="activity_cat_5"> 
                            <div class="p-3 mb-2 bg-info text-white">
                                VST-ECS Contacts (Optional)
                            </div>
                                                    
                            <div class="form-group">
                                <label for="formGroupExampleInput">VST-ECS Engineer</label>
                                <input type="text" class="form-control" name="vst_ecs_engineer" placeholder="Input Engineer Name">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">VST-ECS Product Manager</label>
                                <input type="text" class="form-control" name="vst_ecs_product_manager" placeholder="Input Product Manager Name">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Others</label>
                                <input type="text" class="form-control" name="others" placeholder="Input Others">
                            </div>
                        </div>

                        <div class="activity_cat_6"> 
                            <div class="p-3 mb-2 bg-info text-white">
                                Customer Experience Workshop Details 
                            </div>
                            
                            <div class="form-group">
                                <label for="formGroupExampleInput">Event Name</label>
                                <input type="text" class="form-control" name="event_name" placeholder="Input Event Name">
                            </div>
                        
                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="2020-07-01" name="date_start" id="example-date-input">
                            </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="2020-07-01" name="date_end" id="example-date-input">
                            </div>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Duration</label>
                                <input type="text" class="form-control" name="duration" placeholder="Input Duration">
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Product</label>
                            
                                <div class="form-group col-md-12">
                                    <select id="inputState" class="form-control">
                                        <option selected></option>
                                        <option>Oracle</option>
                                        <option>Cisco</option>
                                        <option>Huawei</option>
                                        <option>VMware</option>
                                        <option>SAP</option>
                                        <option>Pivotal</option>
                                    </select>
                                </div>
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Participants Profile</label>
                            
                                <div class="form-group col-md-12">
                                    <select id="inputState" class="form-control">
                                        <option selected></option>
                                        <option>Technical</option>
                                        <option>Sales</option>
                                        <option>Executives</option>
                                        <option>Mixed</option>
                                    </select>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="formGroupExampleInput">Target No. Attendees</label>
                                <input type="text" class="form-control" name="duration" placeholder="Input Project Name">
                            </div>

                        </div>
                            
                                                                
                        <div class="activity_cat_7"> 
                            <div class="p-3 mb-2 bg-info text-white">
                                Business Partner Development/Workshop 
                            </div>
                            
                            <div class="form-group">
                                <label for="formGroupExampleInput">Event Name</label>
                                <input type="text" class="form-control" name="project_name" placeholder="Input Project Name">
                            </div>
                        
                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                                    <div class="col-10">
                                        <input class="form-control" type="date" value="2020-07-01" name="date_start" id="example-date-input">
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                                    <div class="col-10">
                                        <input class="form-control" type="date" value="2020-07-01" name="date_end" id="example-date-input">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Duration</label>
                                <input type="text" class="form-control" name="duration" placeholder="Input Project Name">
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Product</label>
                            
                                <div class="form-group col-md-12">
                                <select id="inputState" class="form-control">
                                    <option selected></option>
                                    <option>Oracle</option>
                                    <option>Cisco</option>
                                    <option>Huawei</option>
                                    <option>VMware</option>
                                    <option>SAP</option>
                                    <option>Pivotal</option>
                                </select>
                                </div>
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Participants Profile</label>
                            
                                <div class="form-group col-md-12">
                                <select id="inputState" class="form-control">
                                    <option selected></option>
                                    <option>Technical</option>
                                    <option>Sales</option>
                                    <option>Executives</option>
                                    <option>Mixed</option>
                                </select></div>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Target No. Attendees</label>
                                <input type="text" class="form-control" name="number_of_attendees" placeholder="Input Target No of Attendees">
                            </div>

                    
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <input type="checkbox" name="sent_email_response" aria-label="Checkbox for following text input">
                                    Send me an email receipt of my responses                               
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-lg">Submit</button>
                    </form>
                    </x-slot>
                </x-frontend.card>
            </div><!--col-md-10-->
        </div><!--row-->
    </div><!--container-->
@endsection

@push('after-scripts')
    <script>
        // Initial Show fields
        $('.activity_cat_1').hide();
        $('.activity_cat_2').hide();
        $('.activity_cat_3').hide();
        $('.activity_cat_4').hide();
        $('.activity_cat_5').hide();
        $('.activity_cat_6').hide();
        $('.activity_cat_7').hide();

        // Trigger change category
        $(document).on('click', "input[name='activity_category']", function(){

            // Get Value of activity category
            var activityCategory = $("input[name='activity_category']:checked").val();
            console.log(activityCategory);
            // Condition what fields be shown in form

            // Customer experience Workshop
            if (activityCategory == 1){ 
                $('.activity_cat_1').show();
                $('.activity_cat_2').show();
                $('.activity_cat_3').show();
                $('.activity_cat_4').show();
                $('.activity_cat_5').show();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
            }

            // Business Partner Development / Workshop
            if (activityCategory == 2){
                $('.activity_cat_1').hide();
                $('.activity_cat_2').show();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').hide();
                $('.activity_cat_5').show();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
               
            }

            // Virtual POC / POT
            if (activityCategory == 3){
                $('.activity_cat_1').show();
                $('.activity_cat_2').show();
                $('.activity_cat_3').show();
                $('.activity_cat_4').show();
                $('.activity_cat_5').show();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
               
            }

            // iSupport & Education Services
            if (activityCategory == 4){
                $('.activity_cat_2').show();
                $('.activity_cat_5').show();
                $('.activity_cat_1').hide();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').hide();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
               
            }

            // Internal Development / TCT
            if (activityCategory == 5){
                $('.activity_cat_2').show();
                $('.activity_cat_5').show();
                $('.activity_cat_1').hide();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').hide();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
               
            }

        });

    </script>
@endpush