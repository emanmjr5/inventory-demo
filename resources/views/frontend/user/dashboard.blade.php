@extends('frontend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <a href="{{ route('frontend.user.dashboard.request.demo') }}"><button class="btn btn-primary mb-2">Request a Demo</button></a>
                <a href="#"><button class="btn btn-primary mb-2">Guidelines</button></a>
                <x-frontend.card>
                    
                    <x-slot name="header">
                        @lang('Dashboard')
                    </x-slot>

                    <x-slot name="body">
                       
                        <table class="table">
                            <thead class="thead-light">
                              <tr>
                                <th scope="col">Reference Number</th>
                                <th scope="col">Status</th>
                                <th scope="col">Submitted At</th>
                                <th scope="col">Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($requests as $request)
                              <tr>
                                <th scope="row">{{ $request->reference_no}}</th>
                                <td>{{ $request->status}}</td>
                                <td>{{ $request->created_at}}</td>
                                <td><button class="btn btn-primary btn-sm">VIEW DETAILS</button></td>
                              </tr>
                            @endforeach
                            </tbody>
                          </table>
                          {{ $requests->links() }}

                        {{--  <div class="text-center">
                            <p class="text-primary text-size-20"></p>
                            <h2 class="text-dark text-size-50 text-m-size-40">Drive your business forward</b><br><br></h2>
                            <i class="icon-chevron_down text-primary margin-bottom-50 text-size-20"></i> 
                            <img src="{{ asset('img/home/dc2.png') }}" width="900" height="550"/>
                        </div> 

                        <div class="text-center">
                            <br><br>
                            <i class="icon-chevron_down text-primary margin-bottom-50 text-size-20"></i> 
                            <img src="{{ asset('img/home/wipoc.png') }}" width="900" height="550"/>
                        </div>   --}}


            
                    </x-slot>
                </x-frontend.card>
            </div><!--col-md-10-->
        </div><!--row-->
    </div><!--container-->
@endsection
