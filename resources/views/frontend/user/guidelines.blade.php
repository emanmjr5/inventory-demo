@extends('frontend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <x-frontend.card>
                    
                    <y-slot name="header">
                    PROVISIONING GUIDELINES FOR PARTNERS
                    </x-slot>

                    <y-slot name="body">

                    <div class="p-3 mb-2 bg-info text-white">
                    Activity Details 

                    </div>